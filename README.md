# flutter challenge Yusuf

This flutter Challenge is develop IU splash screen, login screen, validation field and show popup

## How to get this project  "Getting Started"

1. git clone this project "https://gitlab.com/ibotaweb/flutter-challenge-yusuf"
2. cd to this project
3. run "flutter clean && flutter pub get"
4. run "flutter run"

## Demo ScreenShot

# ScreenShot1
![App UI](/demo/Screen1.png)

# ScreenShot2
![App UI](/demo/Screen2.png)
